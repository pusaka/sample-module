<?php /*** Bismillahirrahmanirrahim ***/

namespace Apix\Sample;

use Pusaka\Tanur\Support\TanurServiceProvider;

/**
 * ServiceProvider
 */
class ServiceProvider extends TanurServiceProvider
{

    /**
     * Get provided services for deferred service provider
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
