<?php

// route group with `/demo` prefix defined inside `demo` folder
Route::prefix('/demo')->group(function () {
    // route group items
    Route::get('/sample', function () { return 'Hello API!'; });
});